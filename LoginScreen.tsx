import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Text,
  TextInput,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import {StackActions} from '@react-navigation/native';
import Loader from './Loader';

const Logo = require('./assets/logo.jpeg');

const LoginScreen = ({navigation}) => {
  const [showLoading, setShowLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  const [usuario, setUsuario] = useState(null);
  const [contrasenia, setContrasenia] = useState(null);
  const [ciudades, setCiudades] = useState([]);

  const showMessage = (title: string, message: string) => {
    Alert.alert(title, message);
  };

  const onGetCiudades = () => {
    try {
      setShowLoading(true);
      const body = JSON.stringify({
        accion: 'listar',
        estado: 1,
      });

      fetch('http://mobilsoft.ddns.me:3101/admin/ciudades/', {
        method: 'POST',
        body: body,
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          setShowLoading(false);
          const {filas} = responseJson;
          const {recordset} = filas;
          setCiudades(recordset);
        })
        .catch((error) => {
          setShowLoading(false);
          console.log('Error', error);
        });
    } catch (error) {
      setShowLoading(false);
      console.log('Error', error);
    }
  };

  useEffect(() => {
    onGetCiudades();
  }, []);

  const onSubmit = () => {
    if (usuario == null || contrasenia == null) {
      showMessage('ERROR', 'Completa todos los campos requeridos');
    } else {
      try {
        setShowLoading(true);
        const body = JSON.stringify({
          usuario: usuario,
          pass: contrasenia,
        });

        fetch('http://mobilsoft.ddns.me:3101/admin/login/', {
          method: 'POST',
          body: body,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => response.json())
          .then((responseJson) => {
            setShowLoading(false);
            const {filas} = responseJson;
            const {recordset} = filas;
            if (Array.isArray(recordset) && recordset.length === 0) {
              showMessage(
                'Registro no existe',
                'Por favor ingresa un usuario y contraseña correctos',
              );
            } else {
              setShowLoading(false);
              console.warn(JSON.stringify(recordset[0]));
              _storeAsync(recordset[0]);
            }
          })
          .catch((error) => {
            setShowLoading(false);
            showMessage('Error', error.message);
          });
      } catch (error) {
        setShowLoading(false);
        console.log('Error', error);
      }
    }
  };

  const _storeAsync = async (result) => {
    try {
      await AsyncStorage.setItem(
        'codigo_usuario',
        result.codigo_usuario.toString(),
      )
        .then(async () => {
          await AsyncStorage.setItem(
            'nombre_usuario',
            result.nombre_usuario.toString(),
          )
            .then(async () => {
              await AsyncStorage.setItem(
                'ciudad_usuario',
                result.ciudad_usuario.toString(),
              )
                .then(async () => {
                  console.log(
                    ciudades[
                      ciudades
                        .map((element) => element.id)
                        .indexOf(result.ciudad_usuario)
                    ].nombre,
                  );
                  await AsyncStorage.setItem(
                    'nombre_ciudad_usuario',
                    ciudades[
                      ciudades
                        .map((element) => element.id)
                        .indexOf(result.ciudad_usuario)
                    ].nombre,
                  )
                    .then(() => {
                      navigation.dispatch(StackActions.replace('Delivery'));
                    })
                    .catch((error: string) => {
                      showMessage(
                        'error',
                        'There was an error saving the userId' + error,
                      );
                    });
                })
                .catch((error: string) => {
                  showMessage(
                    'error',
                    'There was an error saving the userId' + error,
                  );
                });
            })
            .catch((error: string) => {
              showMessage(
                'error',
                'There was an error saving the userId' + error,
              );
            });
        })
        .catch((error: string) => {
          showMessage('error', 'There was an error saving the userId' + error);
        });
    } catch (error) {
      showMessage('error', error);
    }
  };

  return (
    <>
      <Loader
        visible={showLoading}
        isModal={true}
        isHUD={true}
        color={'#FFFFFF'}
        hudColor={'#000000'}
        barHeight={64}
        size={60}
      />
      <View style={styles.container}>
        <SafeAreaView />
        <View style={styles.containerLogin}>
          <View style={styles.containerLogo}>
            <Image style={styles.logo} source={Logo} />
            <Text style={styles.description}>Bienvenido a Mobilsoft</Text>
          </View>
          <View style={styles.form}>
            <View style={styles.sectionStyle}>
              <Icon name={'account'} size={24} color={'#226fb8'} />
              <TextInput
                style={styles.textInput}
                underlineColorAndroid="transparent"
                value={usuario}
                onChangeText={(text) => {
                  setUsuario(text);
                }}
                placeholderTextColor={'#4F4F4F'}
                placeholder={'Usuario o correo electronico'}
              />
            </View>
            <View style={styles.sectionStyle}>
              <Icon name={'lock'} size={24} color={'#226fb8'} />
              <TextInput
                style={styles.textInput}
                underlineColorAndroid="transparent"
                value={contrasenia}
                onChangeText={(text) => {
                  setContrasenia(text);
                }}
                placeholderTextColor={'#4F4F4F'}
                placeholder={'Contraseña'}
                secureTextEntry={showPassword ? false : true}
              />
              {!showPassword ? (
                <>
                  <TouchableOpacity
                    onPress={() => {
                      setShowPassword(true);
                    }}>
                    <Icon name={'eye'} size={24} color={'#4F4F4F'} />
                  </TouchableOpacity>
                </>
              ) : (
                <>
                  <TouchableOpacity
                    onPress={() => {
                      setShowPassword(false);
                    }}>
                    <Icon name={'eye-off'} size={24} color={'#4F4F4F'} />
                  </TouchableOpacity>
                </>
              )}
            </View>
            <TouchableOpacity style={styles.button} onPress={() => onSubmit()}>
              <Text style={styles.textButton}>Iniciar sesión</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f7f7f7',
  },
  containerLogin: {
    flex: 1,
    marginTop: 16,
  },
  sectionStyle: {
    paddingLeft: 8,
    paddingRight: 8,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#226fb8',
    height: 40,
    borderRadius: 5,
    marginBottom: 16,
  },
  textInput: {
    flex: 1,
    padding: 10,
    color: '#4F4F4F',
  },
  form: {
    marginTop: 16,
    marginLeft: 32,
    marginRight: 32,
  },
  containerLogo: {
    backgroundColor: '#f7f7f7',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 200,
    height: 200,
    margin: 16,
  },
  header: {
    height: 44,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
  },
  buttonBack: {
    marginLeft: 16,
  },
  item: {
    borderTopColor: '#EDEDED',
    borderTopWidth: 1,
    backgroundColor: '#ffffff',
    padding: 16,
  },
  itemBottom: {
    borderBottomColor: '#EDEDED',
    borderBottomWidth: 1,
  },
  itemTitle: {
    fontSize: 15,
    color: '#4F4F4F',
    fontWeight: '600',
    letterSpacing: 0.32,
  },
  itemText: {
    fontSize: 13,
    color: '#8a8a8a',
    letterSpacing: 0.32,
  },
  forgotPasswordContainer: {
    alignSelf: 'flex-end',
  },
  forgotPassword: {
    color: '#4F4F4F',
  },
  button: {
    justifyContent: 'center',
    marginTop: 30,
    height: 44,
    backgroundColor: '#226fb8',
    borderRadius: 5,
  },
  textButton: {
    textTransform: 'uppercase',
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 15,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0.16,
  },
  description: {
    textTransform: 'uppercase',
    fontSize: 18,
    color: '#226fb8',
    padding: 16,
    fontWeight: '600',
    letterSpacing: 0.32,
  },
});

export default LoginScreen;
