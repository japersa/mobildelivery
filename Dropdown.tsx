import React, {useState} from 'react';
import ModalDropdown from 'react-native-modal-dropdown';
import Icon from 'react-native-vector-icons/FontAwesome';

import {View, Text} from 'react-native';

const Dropdown = ({items, onSelect, selectedIndex, placeholder}) => {
  const [opened, setOpened] = useState(false);

  const _openModal = () => {
    setOpened(true);
  };

  const _closeModal = () => {
    setOpened(false);
  };

  return (
    <ModalDropdown
      options={items}
      onDropdownWillShow={_openModal}
      onDropdownWillHide={_closeModal}
      dropdownStyle={{
        shadowColor: '#849094',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowRadius: 5,
        shadowOpacity: 1.0,
      }}
      adjustFrame={(params) => {
        params.left = 32;
        params.right = 32;
        return params;
      }}
      renderRow={(text) => (
        <View style={{paddingHorizontal: 20, paddingVertical: 10}}>
          <Text style={styles.options}>{text}</Text>
        </View>
      )}
      onSelect={onSelect}>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'space-between',
          flexDirection: 'row',
          height: 44,
          color: '#3F3F3F',
          backgroundColor: '#ededed',
          borderRadius: 4,
          paddingStart: 10,
          paddingEnd: 10,
        }}>
        <Text style={styles.text}>
          {selectedIndex > -1 && items[selectedIndex]
            ? items[selectedIndex]
            : placeholder}
        </Text>
        <Icon
          name={opened ? 'angle-up' : 'angle-down'}
          color="#849094"
          size={20}
          style={styles.icon}
        />
      </View>
    </ModalDropdown>
  );
};

const styles = {
  icon: {
    marginLeft: 10,
  },
  text: {
    color: '#bebebe',
  },
  options: {
    color: '#bebebe',
  },
};

export default Dropdown;
