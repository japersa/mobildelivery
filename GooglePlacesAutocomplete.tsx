import React, {useState} from 'react';

import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TextInput,
} from 'react-native';

export const debounce = <F extends (...args: any) => any>(
  func: F,
  waitFor: number,
) => {
  let timeout: number = 0;

  const debounced = (...args: any) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => func(...args), waitFor);
  };

  return debounced as (...args: Parameters<F>) => ReturnType<F>;
};

export default function GooglePlacesAutocomplete({onPress}) {
  const GOOGLE_MAPS_APIKEY = 'AIzaSyBMesj43pD3-xSRRgK7lZLg3kAs3Or6QeQ';

  const [locations, setLocations] = useState([]);
  const [location, setLocation] = useState(null);

  const onChangeDestination = async (search) => {
    const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${GOOGLE_MAPS_APIKEY}&input={${search}}&radius=2000`;
    const result = await fetch(apiUrl);
    const jsonResult = await result.json();
    setLocations(jsonResult.predictions);
  };

  const onChangeDestinationDebounced = debounce(onChangeDestination, 300);

  const onPlacePressed = (place) => {
    setLocations([]);
    onPlaceSelect(place);
  };

  const onPlaceSelect = async (place) => {
    const apiUrl = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${
      place.place_id
    }&key=${GOOGLE_MAPS_APIKEY}&fields=${'formatted_address,geometry,name'}`;
    const result = await fetch(apiUrl);
    const jsonResult = await result.json();
    setLocation(jsonResult.result.formatted_address);
    onPress(jsonResult.result);
  };

  const locationPredictions = locations.map((item) => (
    <TouchableOpacity key={item.id} onPress={() => onPlacePressed(item)}>
      <Text style={styles.locationSuggestion}>{item.description}</Text>
    </TouchableOpacity>
  ));

  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Enter location.."
        style={styles.input}
        value={location}
        onChangeText={(text) => {
          setLocation(text);
          onChangeDestinationDebounced(text);
        }}
      />
      {locationPredictions}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f8f8fc',
  },
  Autocomplete: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  content: {
    paddingLeft: 16,
  },

  inputContainer: {
    flex: 1,
    marginBottom: 8,
  },
  textInput: {
    color: '#8A8A8A',
    fontSize: 11,
    marginBottom: 10,
  },
  input: {
    height: 44,
    color: '#3F3F3F',
    backgroundColor: '#ededed',
    borderRadius: 4,
    paddingStart: 10,
    paddingEnd: 10,
  },
  textArea: {
    height: 70,
    color: '#3F3F3F',
    backgroundColor: '#ededed',
    borderRadius: 4,
    paddingStart: 10,
    paddingEnd: 10,
  },
  locationSuggestion: {
    backgroundColor: 'white',
    padding: 5,
    fontSize: 18,
    borderWidth: 0.5,
    borderColor: '#bebebe',
    color: '#3F3F3F',
  },
});
