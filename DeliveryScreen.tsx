import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  TouchableOpacity,
  Text,
  TextInput,
  Image,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import GooglePlacesAutocomplete from './GooglePlacesAutocomplete';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Dropdown from './Dropdown';
import {ScrollView} from 'react-native-gesture-handler';
import SignatureCapture from 'react-native-signature-capture';
import ImagePicker from 'react-native-image-picker';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import Loader from './Loader';
import {StackActions} from '@react-navigation/native';

const DeliveryScreen = ({navigation}) => {
  const [showLoading, setShowLoading] = useState(false);
  const [identification, setIdentification] = useState(null);
  const [currentDate, setCurrentDate] = useState(null);
  const [date, setDate] = useState(null);

  const [ubicacion, setUbicacion] = useState(null);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [indexSelect, setIndexSelect] = useState(null);
  const [beneficiario, setBeneficiario] = useState(null);
  const [fotoEntrega, setFotoEntrega] = useState(null);
  const [selectOptions, setSelectOptions] = useState([]);
  const [products, setProducts] = useState([]);
  const [cantidad, setCantidad] = useState(null);
  const [firma, setFirma] = useState(null);

  const firmaBeneficiario = useRef(null);

  const [nombreUsuario, setNombreUsuario] = useState(null);
  const [codigoUsuario, setCodigoUsuario] = useState(null);
  const [ciudadUsuario, setCiudadUsuario] = useState(null);
  const [nombreCiudadUsuario, setNombreCiudadUsuario] = useState(null);

  const _bootstrapAsync = async () => {
    setNombreUsuario(await AsyncStorage.getItem('nombre_usuario'));
    setCodigoUsuario(await AsyncStorage.getItem('codigo_usuario'));
    setCiudadUsuario(await AsyncStorage.getItem('ciudad_usuario'));
    setNombreCiudadUsuario(await AsyncStorage.getItem('nombre_ciudad_usuario'));
  };

  useEffect(() => {
    _bootstrapAsync();
  }, []);

  const doLogout = () => {
    Alert.alert(
      'Logout',
      '¿Estás seguro de que quieres cerrar sesión?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Log out',
          onPress: () => {
            let keys = [
              'nombre_usuario',
              'codigo_usuario',
              'ciudad_usuario',
              'nombre_ciudad_usuario',
            ];
            AsyncStorage.multiRemove(keys, (err) => {
              navigation.dispatch(StackActions.replace('Login'));
            });
          },
        },
      ],
      {cancelable: false},
    );
  };

  const launchCamera = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        setFotoEntrega(response.data);
      }
    });
  };

  const onGetProducts = () => {
    try {
      setShowLoading(true);
      const body = JSON.stringify({
        accion: 'listar',
        estado: 1,
        detalle: [],
      });

      fetch('http://mobilsoft.ddns.me:3101/admin/maestros/productos/', {
        method: 'POST',
        body: body,
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          setShowLoading(false);
          const {results} = responseJson;
          const {recordset} = results;
          setProducts(recordset);
          setSelectOptions(recordset.map((elem) => elem.nombre));
        })
        .catch((error) => {
          setShowLoading(false);
          console.log('Error', error);
        });
    } catch (error) {
      setShowLoading(false);
      console.log('Error', error);
    }
  };

  useEffect(() => {
    onGetProducts();
    setCurrentDate(moment(new Date()).format('DD/MM/YYYY'));
    setDate(new Date());
  }, []);

  const showMessage = (title: string, message: string) => {
    setTimeout(() => {
      Alert.alert(title, message);
    }, 200);
  };

  const onSearch = () => {
    try {
      setShowLoading(true);
      const body = JSON.stringify({
        cedula_beneficiario: identification,
        accion: 'tercero',
      });

      fetch('http://mobilsoft.ddns.me:3101/admin/entregas/', {
        method: 'POST',
        body: body,
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          setShowLoading(false);
          const {results} = responseJson;
          const {recordset} = results;

          if (Array.isArray(recordset) && recordset.length === 0) {
            showMessage(
              'Beneficiario no existe',
              'Este beneficiario no ha sido registrado',
            );
          } else {
            setBeneficiario(recordset[0]);
          }
        })
        .catch((error) => {
          setShowLoading(false);
          console.log('Error', error);
        });
    } catch (error) {
      setShowLoading(false);
      console.log('Error', error);
    }
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    setCurrentDate(moment(date).format('DD/MM/YYYY'));
    setDate(date);
    hideDatePicker();
  };

  const changeValueInput = (value: any) => {
    setIdentification(value);
  };

  const saveSign = () => {
    firmaBeneficiario.current.saveImage();
  };

  const _onSaveEvent = (result) => {
    setFirma(result.encoded);
  };

  const onSaveDelivery = () => {
    try {
      const product = products[indexSelect];
      if (
        identification == null ||
        currentDate == null ||
        product == null ||
        cantidad == null ||
        ubicacion == null ||
        firma == null ||
        fotoEntrega == null
      ) {
        showMessage('ERROR', 'Completa todos los campos requeridos');
      } else {
        setShowLoading(true);
        const body = JSON.stringify({
          autorizacion: null,
          cedula_beneficiario: identification,
          fecha_entrega: date,
          id_kits: product.id,
          cant_kits: cantidad,
          id_municipio: ciudadUsuario,
          ubicacion: ubicacion,
          id_funcionario: codigoUsuario,
          firma_beneficiario: 'firma',
          foto_entrega: 'fotoEntrega',
          accion: 'guardar',
        });

        fetch('http://mobilsoft.ddns.me:3101/admin/entregas/', {
          method: 'POST',
          body: body,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson);
            const {results} = responseJson;
            const {recordset} = results;
            if (Array.isArray(recordset) && recordset.length === 0) {
              setShowLoading(false);
              showMessage('Error', 'No se pudo registrar la entrega.');
            } else {
              setShowLoading(false);
              showMessage(
                'Exitoso',
                'La entrega ha sido registrada exitosamente.',
              );
              setBeneficiario(null);
              setIdentification(null);
              setFirma(null);
              setFotoEntrega(null);
              setFirma(null);
              setCantidad(null);
              setIndexSelect(null);
              setUbicacion(null);
            }
          })
          .catch((error) => {
            setShowLoading(false);
            showMessage('Error', 'No se pudo registrar la entrega.');
            console.log('Error', error);
          });
      }
    } catch (error) {
      setShowLoading(false);
      showMessage('Error', 'No se pudo registrar la entrega.');
      console.log('Error', error);
    }
  };

  return (
    <>
      <Loader
        visible={showLoading}
        isModal={true}
        isHUD={true}
        color={'#FFFFFF'}
        hudColor={'#000000'}
        barHeight={64}
        size={60}
      />
      <View style={styles.container}>
        <SafeAreaView style={styles.safeAreaView} />
        <View style={styles.navBar}>
          <TouchableOpacity
            style={styles.buttonBack}
            onPress={() => {
              doLogout();
            }}>
            <Icon name="arrow-left" color={'grey'} size={24} />
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginRight: 16,
              flex: 1,
            }}>
            <View style={[styles.containerTitle, {flex: 1}]}>
              {nombreUsuario != null && (
                <Text style={styles.navBarTitle}>{nombreUsuario}</Text>
              )}
              {nombreCiudadUsuario != null && (
                <Text style={styles.navBarSubtitle}>{nombreCiudadUsuario}</Text>
              )}
            </View>
            <View
              style={[
                styles.inputContainer,
                {marginRight: 8, width: 100, marginTop: 16},
              ]}>
              <Text style={styles.textInput}>Fecha de entrega</Text>
              <TextInput
                style={[styles.input]}
                value={currentDate}
                placeholder={'19/04/2020'}
                editable={false}
                placeholderTextColor={'#3f3f3f'}
              />
            </View>
          </View>
        </View>
        <ScrollView style={{backgroundColor: '#ffffff'}}>
          <View style={[styles.card, {marginTop: 10}]}>
            <View
              style={[
                styles.containerTitleCard,
                {
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                },
              ]}>
              <Text style={styles.cardTitle}>BENEFICIARIO</Text>
              {beneficiario != null && (
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => {
                    setBeneficiario(null);
                    setIdentification(null);
                  }}>
                  <Text style={styles.textButton}>Cambiar</Text>
                </TouchableOpacity>
              )}
            </View>
            <View
              style={{
                margin: 16,
              }}>
              {beneficiario != null ? (
                <View style={{flex: 1}}>
                  <View style={styles.inputContainer}>
                    <Text style={styles.textInput}>Nombres y apellidos</Text>
                    <TextInput
                      style={[styles.input]}
                      value={beneficiario.nombres}
                      placeholder={''}
                      editable={false}
                      placeholderTextColor={'#3f3f3f'}
                    />
                  </View>
                  <View style={styles.inputContainer}>
                    <Text style={styles.textInput}>Cedula</Text>
                    <TextInput
                      style={[styles.input]}
                      value={beneficiario.dni}
                      placeholder={''}
                      editable={false}
                      placeholderTextColor={'#3f3f3f'}
                    />
                  </View>
                </View>
              ) : (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View style={{flex: 1, marginRight: 16}}>
                    <View style={[styles.inputContainer]}>
                      <Text style={styles.textInput}>
                        Identificación a consultar
                        <Text style={styles.requiredLabel}>*</Text>
                      </Text>
                      <TextInput
                        style={styles.input}
                        value={identification}
                        placeholder={'Ej. 12345678'}
                        placeholderTextColor={'#3f3f3f'}
                        onChangeText={(text) => {
                          changeValueInput(text);
                        }}
                        keyboardType={'numeric'}
                      />
                    </View>
                  </View>

                  <TouchableOpacity
                    style={styles.buttonSearch}
                    onPress={() => {
                      onSearch();
                    }}>
                    <Text style={styles.textButtonSearch}>Buscar</Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
          </View>

          <View style={styles.card}>
            <View style={styles.containerTitleCard}>
              <Text style={styles.cardTitle}>DATOS DE LA ENTREGA</Text>
            </View>
            <View style={styles.form}>
              <View style={styles.inputContainer}>
                <Text style={styles.textInput}>
                  Direccion de entrega
                  <Text style={styles.requiredLabel}>*</Text>
                </Text>
                <GooglePlacesAutocomplete
                  onPress={(place: any) => {
                    setUbicacion(place.formatted_address);
                  }}
                />
              </View>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View
                  style={[styles.inputContainer, {flex: 1, marginRight: 16}]}>
                  <Text style={styles.textInput}>
                    Producto o kit <Text style={styles.requiredLabel}>*</Text>
                  </Text>
                  <Dropdown
                    items={selectOptions}
                    selectedIndex={indexSelect}
                    placeholder="Seleccione un producto"
                    onSelect={(index: React.SetStateAction<null>) => {
                      setIndexSelect(index);
                    }}
                  />
                </View>
                <View style={styles.inputContainer}>
                  <Text style={styles.textInput}>
                    Cantidad
                    <Text style={styles.requiredLabel}>*</Text>
                  </Text>
                  <TextInput
                    style={[styles.input, {textAlign: 'right'}]}
                    value={cantidad}
                    placeholder={'0'}
                    onChangeText={(text) => {
                      setCantidad(text);
                    }}
                    placeholderTextColor={'#3f3f3f'}
                    keyboardType={'numeric'}
                  />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.card}>
            <View
              style={[
                styles.containerTitleCard,
                {
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                },
              ]}>
              <Text style={styles.cardTitle}>FIRMA BENEFICIARIO</Text>
              {firma == null ? (
                <>
                  <TouchableOpacity
                    style={[styles.buttonContent, {alignSelf: 'center'}]}
                    onPress={() => {
                      saveSign();
                    }}>
                    <Text style={styles.textButtonContent}>Guardar firma</Text>
                  </TouchableOpacity>
                </>
              ) : (
                <>
                  <TouchableOpacity
                    style={[styles.buttonContent, {alignSelf: 'center'}]}
                    onPress={() => {
                      setFirma(null);
                    }}>
                    <Text style={styles.textButtonContent}>Nueva firma</Text>
                  </TouchableOpacity>
                </>
              )}
            </View>

            <View style={styles.form}>
              {firma == null ? (
                <>
                  <View style={styles.signature}>
                    <SignatureCapture
                      style={{flex: 1}}
                      saveImageFileInExtStorage={false}
                      onSaveEvent={_onSaveEvent}
                      showNativeButtons={false}
                      ref={firmaBeneficiario}
                      showTitleLabel={false}
                      viewMode={'portrait'}
                    />
                  </View>
                </>
              ) : (
                <>
                  <Image
                    style={styles.signature}
                    source={{uri: `data:image/png;base64,${firma}`}}
                  />
                </>
              )}
            </View>
          </View>

          <View style={styles.card}>
            <View
              style={[
                styles.containerTitleCard,
                {
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                },
              ]}>
              <Text style={styles.cardTitle}>EVIDENCIAS</Text>
              <TouchableOpacity
                style={styles.buttonPhoto}
                onPress={() => {
                  launchCamera();
                }}>
                <Text style={styles.textButtonPhoto}>Tomar foto</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.form}>
              <View style={styles.inputContainer}>
                <Text style={styles.textInput}>
                  Foto de entrega
                  <Text style={styles.requiredLabel}>*</Text>
                </Text>
                <View style={{alignItems: 'center'}}>
                  {fotoEntrega != null && (
                    <Image
                      resizeMode="cover"
                      style={{height: 200, width: 200, margin: 16}}
                      source={{
                        uri: `data:image/png;base64,${fotoEntrega}`,
                      }}
                    />
                  )}
                </View>
              </View>
            </View>
          </View>
        </ScrollView>

        <View style={{flex: 1, backgroundColor: '#ffffff'}} />
        <View style={{margin: 16, backgroundColor: '#ffffff'}}>
          <TouchableOpacity
            style={styles.buttonRegister}
            onPress={() => {
              onSaveDelivery();
            }}>
            <Text style={styles.textButtonRegister}>GUARDAR ENTREGA</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  safeAreaView: {
    backgroundColor: '#ffffff',
  },
  navBar: {
    height: 70,
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    alignItems: 'center',
  },
  buttonBack: {
    marginLeft: 16,
  },
  navBarTitle: {
    textTransform: 'uppercase',
    fontSize: 18,
    color: '#4F4F4F',
    fontWeight: '600',
    letterSpacing: 0.32,
  },
  navBarSubtitle: {
    fontSize: 13,
    color: '#4F4F4F',
    letterSpacing: 0.58,
  },
  containerTitle: {
    marginLeft: 8,
  },
  input: {
    height: 44,
    color: '#3F3F3F',
    backgroundColor: '#ededed',
    borderRadius: 4,
    paddingStart: 10,
    paddingEnd: 10,
  },
  inputError: {
    height: 44,
    color: '#3F3F3F',
    borderWidth: 1,
    borderColor: '#ee5435',
    borderRadius: 4,
    paddingStart: 10,
    paddingEnd: 10,
  },
  textInput: {
    color: '#8A8A8A',
    marginBottom: 10,
    fontSize: 11,
  },
  inputContainer: {
    marginBottom: 16,
  },
  requiredLabel: {
    color: 'red',
  },
  buttonPhoto: {
    marginRight: 16,
    marginBottom: 8,
    justifyContent: 'center',
    height: 30,
    width: 100,
    backgroundColor: '#1a5584',
    borderRadius: 5,
  },
  textButtonPhoto: {
    textTransform: 'uppercase',
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 12,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0.16,
  },
  buttonContent: {
    marginRight: 16,
    marginBottom: 8,
    justifyContent: 'center',
    height: 30,
    width: 100,
    backgroundColor: '#1a5584',
    borderRadius: 5,
  },
  textButtonContent: {
    textTransform: 'uppercase',
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 12,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0.16,
  },
  buttonSearch: {
    alignSelf: 'center',
    justifyContent: 'center',
    height: 30,
    width: 100,
    backgroundColor: '#1a5584',
    borderRadius: 5,
  },
  textButtonSearch: {
    textTransform: 'uppercase',
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 12,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0.16,
  },
  button: {
    alignSelf: 'center',
    justifyContent: 'center',
    height: 30,
    width: 100,
    marginRight: 16,
    marginBottom: 8,
    backgroundColor: '#1a5584',
    borderRadius: 5,
  },
  textButton: {
    textTransform: 'uppercase',
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 12,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0.16,
  },
  card: {
    backgroundColor: '#ffffff',
  },
  containerTitleCard: {
    borderBottomWidth: 1,
    borderBottomColor: '#0000000d',
  },
  containerBottomCard: {
    borderTopWidth: 1,
    borderTopColor: '#0000000d',
  },
  cardTitle: {
    paddingRight: 16,
    paddingLeft: 16,
    paddingBottom: 8,
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#4f4f4f',
    fontWeight: '600',
    letterSpacing: 0.32,
  },
  form: {
    padding: 16,
  },
  buttonCancel: {
    marginBottom: 8,
    marginTop: 8,
    marginRight: 16,
    justifyContent: 'center',
    height: 30,
    width: 100,
    backgroundColor: '#bebebe',
    borderRadius: 5,
  },
  textButtonCancel: {
    textTransform: 'uppercase',
    color: '#3f3f3f',
    textAlign: 'center',
    fontSize: 12,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0.16,
  },
  containerButton: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
  buttonRegister: {
    justifyContent: 'center',
    marginTop: 20,
    height: 44,
    backgroundColor: '#1a5584',
    borderRadius: 5,
  },
  textButtonRegister: {
    textTransform: 'uppercase',
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 15,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0.16,
  },
  signature: {
    flex: 1,
    borderColor: '#000033',
    borderWidth: 1,
    height: 150,
  },
});

export default DeliveryScreen;
